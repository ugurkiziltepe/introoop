package sport;

public class Club {

	final static int COUNT = 18;
	public String clubName;
	public String city;
	public String[] team;

	public Club() {
		team = new String[COUNT];
		System.out.println("A club is created!");
	}

	public Club(String clubName, String city) {
		this();
		this.clubName = clubName;
		this.city = city;
	}
}
