
public class Foo {
	static int number;	// a static variable
	int a;
	static {			// a static initialize block - run only once when the class is loaded
		number = 88;
		System.out.println("loading class...");
	}
}
