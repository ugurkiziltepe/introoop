package banking;

public class MainOfAccount {
	public static void main(String[] args) {
		Account account1 = new Account();
		Account account2 = new Account(50.0);

		System.out.println("1)" + account1.balance);
		System.out.println("2)" + account2.balance);

		System.out.println("3)" + account1.totalCount);
		System.out.println("4)" + account2.totalCount);

		// Deposit to only account-1
		account1.addToBalance(40.0);

		System.out.println("5)" + account1.balance);
		System.out.println("6)" + account2.balance);

		System.out.println("7)" + account1.totalCount);
		System.out.println("8)" + account2.totalCount);

		// Deposit to both those accounts
		account1.addToBalance(40.0);
		account2.addToBalance(40.0);

		System.out.println("9)" + account1.balance);
		System.out.println("10)" + account2.balance);

		System.out.println("11)" + account1.totalCount);
		System.out.println("12)" + account2.totalCount);

		// Called static method
		Account.addToTotalCount(10);

		System.out.println("13)" + account1.totalCount);
		System.out.println("14)" + account2.totalCount);

		// access to static variable
		Account.totalCount = 5;

		System.out.println("15)" + account1.totalCount);
		System.out.println("16)" + account2.totalCount);
	}
}
