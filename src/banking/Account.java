package banking;

public class Account {

	public double balance;
	static int totalCount = 0;

	// default constructor
	public Account() {

	}

	// constructor
	public Account(double balance) {
		this.balance = balance;
	}

	public void addToBalance(double deposit) {
		balance += deposit;
		totalCount++;
	}

	public static void addToTotalCount(int inceremention) {
		totalCount += inceremention;
	}

}
