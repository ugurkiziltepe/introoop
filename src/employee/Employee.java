package employee;

public abstract class Employee implements Employable {

	public String name;
	public String lastName;
	public String tcNumber;
	public double salary;

	public Employee() {
	}

	public Employee(String name, String lastName, String tcNumber) {
		this.name = name;
		this.lastName = lastName;
		this.tcNumber = tcNumber;
	}

	public Employee(String name, String lastName, String tcNumber, double salary) {
		this.name = name;
		this.lastName = lastName;
		this.tcNumber = tcNumber;
		this.salary = salary;
	}

}
