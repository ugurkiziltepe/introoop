package employee;

public interface Employable {

	final static String NATIONALITY = "TC";
	final static int MAX_AGE = 30;

	public abstract double getMonthlyIncome();

	public abstract void work();
}
