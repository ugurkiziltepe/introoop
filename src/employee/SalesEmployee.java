package employee;

public class SalesEmployee extends Employee {

	public double bonus;

	public SalesEmployee(String name, String lastName, String tcNumber, double salary, double bonus) {
		super(name, lastName, tcNumber, salary);
		this.bonus = bonus;
	}

	@Override
	public double getMonthlyIncome() {
		double income = salary;
		return income + bonus;
	}

	@Override
	public void work() {
		System.out.println("Would you like a little bit software?");
	}

}
