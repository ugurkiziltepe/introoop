package employee;

public class PartTimeEmployee extends Employee {

	public int workingHour;
	public double hourlyRate;

	public PartTimeEmployee(String name, String lastName, String tcNumber, int workingHour, double hourlyRate) {
		super(name, lastName, tcNumber);
		this.workingHour = workingHour;
		this.hourlyRate = hourlyRate;
	}

	@Override
	public double getMonthlyIncome() {
		return hourlyRate * workingHour;
	}

	@Override
	public void work() {
		System.out.println("I'm writing intern notes!");
	}
}
