package student;

public class School {

	public static void main(String[] args) {
		Student std1 = new Student();
		Student std2; // declare a Student reference
		std2 = new Student(); // assign the reference to a Student Object

		std1.studentName = "Ugur Kiziltepe";
		std1.exam1 = 50;
		std1.exam2 = 60;

		std2.studentName = "Ali Can";
		std2.exam1 = 30;
		std2.exam2 = 50;

		System.out.println("std1: " + std1.studentName + " - " + std1.average());
		System.out.println("std2: " + std2.studentName + " - " + std2.average());

	}

}

