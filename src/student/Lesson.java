package student;

public class Lesson {
	public String name;
	public int code;
	public String superVisor;

	public Lesson(String name, int code, String superVisor) {
		this.name = name;
		this.code = code;
		this.superVisor = superVisor;
	}

}
