package student;

public class Student {
	public String studentName;
	public int studentNumber;
	public double exam1;
	public double exam2;
	public Lesson lesson = new Lesson("History", 1001, "Ali Can");

	public double average() {
		double avg;
		avg = (exam1 + this.exam2) / 2;
		return avg;
	}

}
