import employee.Employee;
import employee.PartTimeEmployee;
import employee.SalesEmployee;

public class Main {

//	public static void main(String[] args) {
//		Employee developer, intern, salesman;
//
//		developer = new Employee("ugur", "kiziltepe", "115599", 1000.0);
//		intern = new PartTimeEmployee("ali", "can", "775533", 20, 10.0);
//		salesman = new SalesEmployee("veli", "can", "665544", 1000.0, 200);
//
//		System.out.println(developer.name + " - total income: " + developer.getMonthlyIncome());
//		System.out.println(intern.name + " - total income: " + intern.getMonthlyIncome());
//		System.out.println(salesman.name + " - total income: " + salesman.getMonthlyIncome());
//
//
//	}

	
	public static void main(String[] args) {

		Employee employee = new PartTimeEmployee("ali", "can", "775533", 20, 10.0);

		if (employee instanceof SalesEmployee) {
			System.out.println("You'll get $1000 bonus!");

		} else if (employee instanceof PartTimeEmployee) {
			System.out.println("Work! Money? No money!");
		}
	}
}
