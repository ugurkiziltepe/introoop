package customer;

public class Personal extends Customer {
	public String tcNumber;

	public Personal(String tcNumber, String name, String accountNumber, double balance) {
		super(name, accountNumber, balance);
		this.tcNumber = tcNumber;
	}

}
