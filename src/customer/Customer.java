package customer;

public class Customer {
	public String name;
	public String accountNumber;
	private double balance;

	public Customer() {

	}

	public Customer(String name, String accountNumber, double balance) {
		this.name = name;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public double getBalance() {
		return balance;
	}

	public void deposit(double amount) {
		if (amount > 0.0)
			this.balance += amount;
	}

	public void withdraw(double amount) {
		if (amount > 0.0)
			this.balance -= amount;
	}

}
