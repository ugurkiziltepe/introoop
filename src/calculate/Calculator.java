package calculate;

public class Calculator {

	private int n1 = 5;
	private int n2 = 10;

	public void add() {
		int result = n1 + n2;
		System.out.println(result);
	}

	public void add(int s1, int s2) {
		int result = s1 + s2;
		System.out.println(result);
	}

	public long add(long s1, long s2) {
		long result = s1 + s2;
		return result;
	}
}


